from django import forms
from .models import Project , ProjectImage
from django.forms import modelformset_factory


class AddProjectForm(forms.ModelForm):

    class Meta:
        model = Project
        # fields = ['start_date']
        exclude = ['creation_date', 'user']
        widgets = { "start_date" : forms.widgets.DateInput(attrs={'type': 'date'}) ,
                     "end_date" : forms.widgets.DateInput(attrs={'type': 'date'}) ,
                       "details" : forms.Textarea()}
        # widgets = { "start_date" :forms.DateInput()}
        # widget=forms.widgets.DateInput(attrs={'type': 'date'})

class ImageForm(forms.ModelForm):
    project_image = forms.ImageField(label='Image')    
    class Meta:
        model = ProjectImage
        fields = ('project_image', )
ImageFormSet = modelformset_factory(ProjectImage,
                                        form=ImageForm, extra=3)




