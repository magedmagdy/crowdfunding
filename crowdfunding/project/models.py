from django.db import models, connection
from django.contrib.auth.models import User
from random import randint

DEFAULT_PROJECT_HEADER_IMAGE = "/media/project_pics/default.jpg"
class Category(models.Model):
    name = models.CharField(max_length=50)
    
    def __str__(self):
        return self.name

    def get_projects_count(self):
        self.projects_count = self.project_set.count
        return self.projects_count

class Tag(models.Model):
    name = models.CharField(max_length=50)
    
    def __str__(self):
     
        return self.name

class Project(models.Model):
    title = models.CharField(max_length=100, unique=True)
    details = models.CharField(max_length=100)
    target = models.IntegerField()
    start_date = models.DateField(null=True)
    end_date = models.DateField(null=True)
    creation_date = models.DateField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tag, related_name ="projects")

    def __str__(self):
        return self.title

    def get_avg_rate(self):
        return self.rate_set.all().aggregate(models.Avg("value"))['value__avg'] or 0

    def get_total_donations(self):
        return self.donation_set.all().aggregate(models.Sum("value"))['value__sum'] or 0

    def get_rates_count(self):
        return self.rate_set.all().count()
    
    def get_donations_count(self):
        return self.donation_set.all().count()
    
    def is_cancelable(self):
        return self.get_total_donations() <= 0.25 * self.target

    def get_images(self):
        return self.projectimage_set.all()
    
    def get_header_image(self):
        img = self.projectimage_set.first()
        return img.project_image.url if img else DEFAULT_PROJECT_HEADER_IMAGE
    
    def get_rate(self, user_id):
        rate = self.rate_set.filter(user_id=user_id)
        return rate[0].value if rate else 0

    def get_reports_count(self):
        return self.report_set.count()

    def is_reported_by(self, user_id):
        return True if self.report_set.filter(user_id=user_id) else False
    @staticmethod
    def format_project_data(projects):
        projects_data = []
        for project in projects:
            header_image = project.get_header_image()
            target = project.target
            projects_data.append({
                "project_id": project.id,
                "project_title": project.title,
                # "project_rate": round(project.get_avg_rate()),
                "project_rate": (project.get_avg_rate()),
                "creation_date": project.creation_date,
                "header_image": header_image,
                "target": target,
                "percentage": int((project.get_total_donations() / target) * 100),
                "num_rates": project.get_rates_count()
            })
        return projects_data

    @classmethod
    def get_top_five_rated(cls):
        #Project.objects.values("title", "id", "creation_date").annotate(avg=Avg("rate__value")).order_by("-avg")[:5]
        cursor = connection.cursor()
        cursor.execute("select p.id pid, p.title title, avg(value) avg_rate, creation_date from project_project p join project_rate r on p.id = r.project_id group by p.id order by 'avg_rate' desc limit 5")
        projects = cursor.fetchall()
        projects_data = []
        for project in projects:
            header_image = Project.objects.get(id=project[0]).get_header_image()
            projects_data.append({
                "project_id": project[0],
                "project_title": project[1],
                "project_rate": round(project[2]),
                "creation_date": project[3],
                "header_image": header_image if header_image else DEFAULT_PROJECT_HEADER_IMAGE,
                "num_rates": Project.objects.get(pk=project[0]).get_rates_count()
            })
        return projects_data

    @classmethod
    def get_last_five_created(cls):
        projects = Project.objects.all().order_by("-creation_date")[:5]
        return Project.format_project_data(projects)
        #cls.objects._.order_by('id')[:5]
    
    @classmethod
    def get_most_recommended_five(cls):
        "Select 5 random projects from all projects"
        ids = []
        while len(ids) < 5:
            rand = randint(0, Project.objects.count() - 1)
            if rand not in ids:
                ids.append(rand)
        projects = [Project.objects.all()[pid] for pid in ids]
        return Project.format_project_data(projects)

    def get_similar_five(self):
        return Project.objects.filter(tags__in=self.tags.all()).values("id", "title").annotate(count=models.Count("id")).order_by("-count")[:5]

class Donation(models.Model):
    value = models.PositiveIntegerField() 
    donation_date = models.DateField(auto_now_add=True)  
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username + " donated to '" + self.project.title + "' by " + str(self.value)

class Rate(models.Model):
    class Meta:
        unique_together = (('user', 'project'),)

    value = models.IntegerField(choices = [(i, str(i)) for i in range(1, 6)])
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username + " Rated '" + self.project.title + "' with " + str(self.value)


class ProjectImage(models.Model):
    project_image = models.ImageField(upload_to="project_pics")
    project = models.ForeignKey(Project, on_delete=models.CASCADE)

    def __str__(self):
        return self.project_image.url



