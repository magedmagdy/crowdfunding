"""fund URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views


#rate
#report
#delete project
#reset pass
#activation




urlpatterns = [
   # urls in project App
    path('',views.index, name="all_projects"),
    path('<int:id>/',views.project, name="project"),
    path('<int:id>/donate/', views.donate, name="donate"),
    path('<int:id>/cancel/', views.cancel_project, name="cancel_project"),
    path('<int:id>/report/', views.report_project, name="report_project"),
    path('<int:id>/rate/', views.rate_project, name="rate_project"),
    path('add/',views.add_project , name = "add_project"),
    path('list/',views.index, name="list_all_projects"),
    path('<str:category_name>/',views.category, name="category_project"),
    path('<int:id>/comment/add/', views.add_comment, name="add_comment"),
    path('comment/<int:comment_id>/delete/', views.delete_comment, name="delete_comment"),


]
    
