from django.shortcuts import render, redirect, reverse
from .models import *
from django.contrib.auth.decorators import login_required
from user.models import Comment, Report
from django.forms import modelformset_factory
from django.contrib import messages
from django.http import HttpResponse,HttpResponseRedirect
from .forms import *
from django.core.exceptions import ObjectDoesNotExist
from django.db.utils import IntegrityError
# Create your views here.

message = None
def index(request):
    categories = Category.objects.all()
    for category in categories:
        category.get_projects_count()
    return render(request,'project/list_category.html', {'categories': categories})


def category(request, category_name):
    projects = Project.objects.filter(category__name = category_name)
    return render(request,'project/list_project.html', {'projects': Project.format_project_data(projects)})

def project(request, id):
    project = Project.objects.get(id=id)
    comments = project.comment_set.all()
    context = {
        'project' : project,
        'comments' : project.comment_set.all(),
        'tags' : project.tags.all(),
        'images': project.get_images(),
        'header_image' : project.get_header_image(),
        'categories': Category.objects.all(),
        "is_cancelable": project.is_cancelable(),
        "avg_rate": round(project.get_avg_rate()),
        "range": range(1, 6),
        "num_rates": project.get_rates_count(),
        "total_donations": project.get_total_donations(),
        "num_donation": project.get_donations_count(),
        "similar_projects": project.get_similar_five(),
        "rate_value": project.get_rate(request.user.id), 
        "rates_range": range(1, 6)
        }
    if message:
        context["message"] = message
    return render(request ,'project/project.html', context)



@login_required(login_url="/login")
def add_comment(request, id):
    if request.method == "POST":
        comment_txt = request.POST.get("comment")
        if comment_txt:
            comment = Comment(content=comment_txt)
            comment.user = request.user
            comment.project_id = id
            comment.save()
    return redirect(reverse("project", kwargs={"id":id}))     

@login_required(login_url="\login")
def delete_comment(request, comment_id):
    comment = Comment.objects.get(id=comment_id)
    if request.user.id == comment.user_id:
        project_id = comment.project_id
        comment.delete()
        return redirect(reverse("project", kwargs={"id": project_id}))
    return redirect(reverse("index"))

@login_required(login_url="/login")
def donate(request, id):
    if request.method == "POST":
        donation_value = request.POST.get("donation_value")
        donation = Donation()
        donation.value = donation_value
        donation.user = request.user
        donation.project = Project.objects.get(id=id)
        donation.save()
        messages.success(request, "Your donation is submitted succeessfully!")
    return redirect(reverse("project", kwargs={"id": id}))

@login_required(login_url="/login")
def add_project(request):

#     ImageFormSet = modelformset_factory(ProjectImage,
#                                         form=ImageForm, extra=3)
    #'extra' means the number of photos that you can upload   ^
    if request.method == 'POST':
        postForm = AddProjectForm(request.POST)
        formset = ImageFormSet(request.POST, request.FILES,
                               queryset=ProjectImage.objects.none())
        if postForm.is_valid() and formset.is_valid():
            post_form = postForm.save(commit=False)
            post_form.user = request.user
          #   post_form.user = request.user
            post_form.save()
            for form in formset.cleaned_data:
                #this helps to not crash if the user   
                #do not upload all the photos
                if form:
                    imageForm = form['project_image']
                    photo = ProjectImage(project=post_form, project_image=imageForm)
                    photo.save()
            messages.success(request,
                             "Yeeew, check it out on the home page!")
            return HttpResponseRedirect("/")
        else:
            print(postForm.errors, formset.errors)
    else:
        postForm = AddProjectForm()
        formset = ImageFormSet(queryset=ProjectImage.objects.none())
    return render(request, 'project/addproject.html',
                  {'postForm': postForm, 'formset': formset})


@login_required(login_url="/login")
def report_project(request, id):
    project = Project.objects.filter(pk=id)
    if not project: #check to see if the project exists
        messages.warning(request, "Project not found!")
        return redirect(reverse('index'))
    if project[0].is_reported_by(request.user.id): #check if user has reported the project before
        messages.warning(request, "You have already reported this project before!")
        return redirect(reverse('index'))
    report = Report(user_id=request.user.id, project_id=id)
    report.save()

    #delete the project if it has recieved 10 reports or more
    if project[0].get_reports_count() >= 10:
        project[0].delete()
    messages.success(request, "your report has been recorded!")
    return redirect(reverse('index'))

@login_required(login_url="/login")
def rate_project(request, id):
    if request.method == "POST":
        rate_value = request.POST.get("rate_value")
        rate = Rate.objects.filter(user_id=request.user.id, project_id=id)
        if rate:
            rate.update(value=rate_value)
            messages.success(request, "Your rating has been updated successfully!")
        else:
            rate = Rate()
            rate.value = rate_value
            rate.user = request.user
            rate.project = Project.objects.get(id=id)
            rate.save()
            messages.success(request, "Your rating is submitted succeessfully!")
    return redirect(reverse("project", kwargs={"id": id}))

@login_required(login_url="/login")
def cancel_project(request, id):
    try :
        project = Project.objects.get(id=id)
        if request.user.id == project.user_id and project.is_cancelable():
            project.delete()
            messages.info(request, "The project has been canceled!")
            return redirect(reverse("index"))  
        messages.warning(request, "Something went wrong!") 
        return redirect(reverse("index"))   
    except ObjectDoesNotExist:     
        messages.info(request, "Project not found!")
        return redirect(reverse("index"))   