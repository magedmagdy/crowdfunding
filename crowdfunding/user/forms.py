from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
import re

def validate_egyptian_phone(value):
    if re.search(r"^01[0-25][\d]{8}$", value) is None:
        raise ValidationError("Not valid Phone Number.")
    return value


class RegisterationForm(UserCreationForm):
    phone = forms.CharField(max_length=15, required=True, validators=[validate_egyptian_phone])
    profile_pic = forms.ImageField(required=False)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )
    
class EditeProfileForm(forms.Form):
    username = forms.CharField(max_length=15, required=True)
    first_name = forms.CharField(max_length=15, required=True)
    last_name = forms.CharField(max_length=15, required=True)
    phone = forms.CharField(max_length=15, required=True, validators=[validate_egyptian_phone])
    profile_pic = forms.ImageField(required=False)
    fb_profile_url = forms.URLField(required=False, max_length=200)
    country = forms.CharField(max_length=15, required=False)
    birth_date = forms.DateField(required=False)

"""     class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name',)
        exclude = ("password1", "password2") """
    
class ProfileForm(UserCreationForm):
    pass