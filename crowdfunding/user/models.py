from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from project.models import Project

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.CharField(max_length=15)
    fb_profile_url = models.URLField(null=True, blank=True, max_length=200)
    country = models.CharField(default='Egypt', blank=True, null=True ,max_length=50)
    birth_date = models.DateField(null=True)
    profile_pic = models.ImageField(upload_to="profile_pics", default="profile_pics/default.jpg")

    '''
    Creating a signal so when an instance of User is being saved into DB an instance of UserProfile will be created and
    associated with the created User Instance and then save the profile instance.'''


    def get_all_projects(self):
        return self.project_set.all()

    def __str__(self):
        return self.user.username



class Comment (models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    date = models.DateField( auto_now_add=True)
    content = models.TextField()

    def __str__(self):
        return str(self.user_id) + " commented on " + str(self.project_id)



class Report (models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)

    def __str__(self):
        return self.user_id + " reported " + self.project_id

