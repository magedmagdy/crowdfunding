from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.db.models import F


import random
import string

def get_random_password(string_length=10):
    password_characters = string.ascii_letters + string.digits
    return ''.join(random.choice(password_characters) for i in range(string_length))


def login_required(func=None, redirect_url=None):

    def _decorator(func):
        def _wrapper(request, *args, **kwargs):
            if request.session.get("logged_id", None):
                return func(request, *args, **kwargs)
            if redirect_url:
                return redirect()
        return _wrapper
    
    if func:
        return _decorator(func)
    return _decorator

