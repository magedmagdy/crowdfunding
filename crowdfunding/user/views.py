from django.shortcuts import render, reverse, redirect
from django.contrib.auth import login, logout, authenticate 
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from project import models as p_models
from .models import UserProfile
from .forms import RegisterationForm, EditeProfileForm
import re
from django.db.models import Q
from .utils import get_random_password


def index(request):
    context = {
        "top_rated_five": p_models.Project.get_top_five_rated(),
        "last_created_five": p_models.Project.get_last_five_created(),
        "recommended_five": p_models.Project.get_most_recommended_five(),
        "categories": p_models.Category.objects.all()
    }
    return render(request, "user/home.html", context)


def register(request):
    if request.method == 'POST':
        form = RegisterationForm(request.POST, request.FILES)
        if form.is_valid():
            user = form.save()
            #user.refresh_from_db()  #load the profile instance created by the signal
            profile = UserProfile.objects.create(user=user)
            profile.phone = form.cleaned_data.get("phone")
            if form.cleaned_data.get("profile_pic"):
                profile.profile_pic = form.cleaned_data.get("profile_pic")
            user.userprofile.save()
            password = form.cleaned_data.get("password1")
            user = authenticate(username=user.username, password=password)
            if user:
                login(request, user)
                return redirect(reverse("home"))
            return render(request, "user/login.html", {'form': form, "categories": p_models.Category.objects.all()})
        else:
            return render(request, "user/register.html", {'form': form, "categories": p_models.Category.objects.all()})
    else:
        form = RegisterationForm()
    return render(request, 'user/register.html', {'form': form, "categories": p_models.Category.objects.all()})


def login_user(request):
    if request.method == "POST":
        #if request.user.is_a
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return redirect(reverse("home"))
        return render(request, "user/login.html", {"categories": p_models.Category.objects.all()})
    else:
        return render(request, "user/login.html", {"categories": p_models.Category.objects.all()})


@login_required(login_url='/login')
def logout_user(request):
    logout(request)
    return redirect(reverse("index"))


def profile(request, username):
    user = User.objects.get(username=username)
    projects = [
        {"project_obj" : project, "header_image": project.get_header_image()} for project in user.project_set.all()
    ]
    context = {
        "profile_user_name": user.first_name + " " + user.last_name,
        "user": user,
        "profile": user.userprofile,
        "date_joined": user.date_joined.date(),
        "projects": projects,
        "donations": user.donation_set.all(),
        "categories": p_models.Category.objects.all(),
        "form": EditeProfileForm()
    }
    return render(request, "user/profile.html", context)


@login_required(login_url="/login")
def delete_profile(request):
    user = request.user
    logout(request)
    user.delete()
    return redirect(reverse("index"))


@login_required(login_url="/login")
def edit_profile(request):
    if request.method == 'POST':
        print(request.FILES)
        form = EditeProfileForm(request.POST, request.FILES)
        if form.is_valid():
            #user = form.save()
            #print(user)
            #user.refresh_from_db()  #load the profile instance created by the signal
            profile = request.user.userprofile#UserProfile.objects.create(user=user)
            profile.phone = form.cleaned_data.get("phone")
            if form.cleaned_data.get("profile_pic"):
                profile.profile_pic = form.cleaned_data.get("profile_pic")
            request.user.userprofile.save()
            request.user.username = form.cleaned_data.get("username")
            request.user.first_name = form.cleaned_data.get("first_name")
            request.user.last_name = form.cleaned_data.get("last_name")
            request.user.save()

            return render(request, "user/profile.html", {'form': form, "categories": p_models.Category.objects.all()})
        else:
            print(form.errors)
            return redirect(reverse("profile", kwargs={
                "username": request.user.username}))
        return redirect(reverse("profile", kwargs={
            "username": request.user.username}))
    else :
        return render(request, "user/edit_profile.html", {user: request.user})
        
    

def search(request):
    query_param = request.GET.get("q")
    if query_param:
        #get the projects with title matches the search string excluding the tags (strings prefixed with #).
        parsed_query = "".join(re.split("#\w+", query_param))
        projects_contains_titles = p_models.Project.objects.filter(title__icontains=parsed_query)
        #get all tags and get all projects the have either on of those tags
        tags = re.findall("#(\w+)", query_param)
        projects_contains_tags = []
        if tags:
            q = Q(tags__name__icontains = tags[0])
            for tag in tags[1:]:
                q |= Q(tags__name__icontains=tag)
            projects_contains_tags = p_models.Project.objects.filter(q).distinct()
        projects = set(list(projects_contains_tags) + list(projects_contains_titles))
        for project in projects:
            project.header_img = project.get_header_image()
            project.rate = project.get_avg_rate()
        #return a unique set of all projects
        context = {
            "projects": projects,
            "categories": p_models.Category.objects.all()
        }
        return render(request, 'project/results.html', context)

def password_recover(request):
    ''' Don't touch this it's not working '''
    if request.method == "POST":
        try: 
            from django.core.mail import send_mail
        except ImportError:
            return redirect(reverse("index"), message="Failed to send email.")
        username = request.POST.get("username")
        new_password = get_random_password(30)
        User.objects.get(username=username).set_password(password)
        send_mail(
            'Password Reset',
            'Here is the message.',
            'from@example.com',
            ['to@example.com'],
            fail_silently=False,
        )
        return redirect()
    return render(request, "user/password_reset_confirm.html")